﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class Common
    {
        public const string LogCategory = "Bulges";

        public static TweakTextureDef TweakTextureDef = DefDatabase<TweakTextureDef>.GetNamed("Default", false);
    }
}
