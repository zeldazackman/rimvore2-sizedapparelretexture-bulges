﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_Multi : PreyDrawerEntry
    {
        int preyCount = 2;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            if(records.Count != preyCount)
            {
                return $"Not corrent number of prey, needs: {preyCount}, is: {records.Count}";
            }
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (preyCount < 0)
                yield return $"Field {nameof(preyCount)} must be at least 2 - is currently {preyCount}";
        }
    }
}
