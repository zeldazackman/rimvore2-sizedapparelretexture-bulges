﻿using RimVore2;
using System.Collections.Generic;
using UnityEngine.Profiling;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry
    {
        private List<GraphicData> graphics;
        private List<PreyDrawerEntry> subEntries;

        public virtual AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            return true;
        }

        public IEnumerable<GraphicData> GetGraphics(List<VoreTrackerRecord> records)
        {
            AcceptanceReport report = AppliesTo(records);
            if (RV2Log.ShouldLog(true, Common.LogCategory))
                RV2Log.Message($"Entry of type {GetType()} accepted ? {(report.Accepted ? "YES" : report.Reason)}", Common.LogCategory);
            if (!report.Accepted)
            {
                yield break;
            }
            foreach (GraphicData graphicData in GetGraphicDataFromSelf())
            {
                yield return graphicData;
            }
            foreach (GraphicData graphicData in GetGraphicDataFromSubEntries(records))
            {
                yield return graphicData;
            }
            foreach (GraphicData graphicData in GetAdditionalGraphicData(records))
            {
                yield return graphicData;
            }
        }

        protected IEnumerable<GraphicData> GetGraphicDataFromSelf()
        {
            if (!graphics.NullOrEmpty())
            {
                foreach (GraphicData graphic in graphics)
                {
                    yield return graphic;
                }
            }
        }

        protected IEnumerable<GraphicData> GetGraphicDataFromSubEntries(List<VoreTrackerRecord> records)
        {
            if (!subEntries.NullOrEmpty())
            {
                foreach (PreyDrawerEntry entry in subEntries)
                {
                    foreach (GraphicData subGraphic in entry.GetGraphics(records))
                    {
                        yield return subGraphic;
                    }
                }
            }
        }

        protected virtual IEnumerable<GraphicData> GetAdditionalGraphicData(List<VoreTrackerRecord> records)
        {
            yield break;
        }

        public virtual IEnumerable<string> ConfigErrors()
        {
            if (graphics.NullOrEmpty() && subEntries.NullOrEmpty())
                yield return $"Either {nameof(graphics)} or {subEntries} must be set";
        }
    }
}
