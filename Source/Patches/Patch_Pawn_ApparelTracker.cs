﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    [HarmonyPatch(typeof(Pawn_ApparelTracker), nameof(Pawn_ApparelTracker.Notify_ApparelChanged))]
    public static class Patch_Pawn_ApparelTracker
    {
        [HarmonyPostfix]
        public static void RecacheHiddenGraphicsDueToApparelChanges(Pawn ___pawn)
        {
            ___pawn.GetBulgePawnData()?.drawer?.Recache();
        }
    }
}
