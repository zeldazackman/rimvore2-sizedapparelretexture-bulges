﻿using RV2_SAR_Bulges;
using Verse;

namespace RV2_SAR_Bulges
{
    public class BulgePawnData
    {
        public Pawn pawn;
        public VoreTrackerDrawer drawer;

        public BulgePawnData(Pawn pawn)
        {
            this.pawn = pawn;
            drawer = new VoreTrackerDrawer(pawn);
        }
    }
}
