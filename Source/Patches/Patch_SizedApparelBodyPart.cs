﻿using HarmonyLib;
using SizedApparel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RV2_SAR_Bulges.Patches
{
    [HarmonyPatch(typeof(SizedApparelBodyPart), nameof(SizedApparelBodyPart.GetDepthOffsetSouth))]
    public static class Patch_SizedApparelBodyPart
    {
        const float hornyAboveShirtDepthOffsetSouthOverwrite = 0.0108f;

        //public static MethodBase TargetMethod()
        //{
        //    return AccessTools.Constructor(typeof(SizedApparelBodyPart));
        //}

        [HarmonyPrefix]
        public static void OverwriteOffsets(SizedApparelBodyPart __instance)
        {
            __instance.hornyAboveShirtDepthOffsetSouth = hornyAboveShirtDepthOffsetSouthOverwrite;
        }
    }
}
