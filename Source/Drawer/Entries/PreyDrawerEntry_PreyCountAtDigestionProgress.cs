﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_PreyCountAtDigestionProgress : PreyDrawerEntry
    {
        int preyCount = 0;
        FloatRange digestionProgressRange = FloatRange.Zero;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            List<float> progressList = records
                .Select(record => DrawerRequestUtility.GetDigestionProgress(record))
                .ToList();

            int actualPreyCount = progressList.Count(progress => digestionProgressRange.Includes(progress));
            if(actualPreyCount != preyCount)
            {
                return $"Not corrent number of prey, needs: {preyCount} in range {digestionProgressRange.min}~{digestionProgressRange.max}, is: {actualPreyCount} ({progressList.ToStringSafeEnumerable()})";
            }
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (preyCount < 0)
                yield return $"Field {nameof(preyCount)} must be at least 0 - is currently {preyCount}";
        }
    }
}
