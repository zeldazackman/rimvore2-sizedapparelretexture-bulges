﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class PawnDataManager
    {
        /// <summary>
        /// This data doesn't need to be persisted, so simply populate on request, don't scribe
        /// </summary>
        static Dictionary<Pawn, BulgePawnData> cachedPawnData = new Dictionary<Pawn, BulgePawnData>();

        public static BulgePawnData GetBulgePawnData(this Pawn pawn)
        {
            BulgePawnData pawnData = cachedPawnData.TryGetValue(pawn);
            if(pawnData == null)
            {
                pawnData = new BulgePawnData(pawn);
                cachedPawnData.Add(pawn, pawnData);
            }
            return pawnData;
        }
    }
}
