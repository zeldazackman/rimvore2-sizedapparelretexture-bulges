﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_DigestionProgress : PreyDrawerEntry
    {
        int progress = -1;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            VoreStage currentStage = records.First().CurrentVoreStage;

            PartGoalExtension_DigestionProgress extension = currentStage?.def?.partGoal?.GetModExtension<PartGoalExtension_DigestionProgress>();
            if(extension == null)
            {
                return $"Could not retrieve {nameof(PartGoalExtension_DigestionProgress)} extension for vore stage {currentStage?.def?.defName}";
            }
            return progress == extension.digestionProgress;
        }


        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (progress < 0)
                yield return $"field {nameof(progress)} must be non-negative";
        }
    }
}
