﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class HashUtility
    {
        public static int CombineHashes(int seed, params object[] objects)
        {
            foreach (object obj in objects)
            {
                seed = Gen.HashCombine(seed, obj);
            }
            return seed;
        }
    }
}
