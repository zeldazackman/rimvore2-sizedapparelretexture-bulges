﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_Hediff : PreyDrawerEntry
    {
        HediffDef hediff;
        FloatRange? severityRange = null;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            List<Hediff> hediffs = records.FirstOrDefault()?.Prey?.health?.hediffSet?.hediffs;
            if(hediffs.NullOrEmpty())
            {
                return $"No hediffs";
            }
            List<Hediff> matchingHediffs = hediffs
                .Where(hed => hed.def == hediff)
                .ToList();
            if (matchingHediffs.NullOrEmpty())
            {
                return $"No hediff of def {hediff.defName}";
            }
            if(severityRange != null)
            {
                bool anyHediffInSeverityRange = matchingHediffs.Any(hed => severityRange.Value.Includes(hed.Severity));
                if (!anyHediffInSeverityRange)
                {
                    return $"No hediff of def {hediff} in range {severityRange}";
                }
            }
            return true;
        }


        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (hediff == null)
                yield return $"Required field \"{nameof(hediff)}\" is not set";
        }
    }
}
