﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class TweakTextureDef : Def
    {
        List<string> texPathsToTweak = new List<string>();

        public bool AppliesTo(GraphicData graphicData)
        {
            return AppliesTo(graphicData.texPath);
        }
        public bool AppliesTo(string texPath)
        {
            return texPathsToTweak.Contains(texPath);
        }
    }
}
