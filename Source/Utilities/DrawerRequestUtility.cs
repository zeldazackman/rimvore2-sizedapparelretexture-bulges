﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public static class DrawerRequestUtility
    {
        public static Dictionary<VoreDrawerDef, List<VoreTrackerRecord>> MakeDrawersWithRequests(VoreTracker tracker)
        {
            Dictionary<VoreDrawerDef, List<VoreTrackerRecord>> entries = new Dictionary<VoreDrawerDef, List<VoreTrackerRecord>>();
            DrawerDefRequest drawerDefRequest;
            foreach (VoreTrackerRecord record in tracker.VoreTrackerRecords)
            {
                drawerDefRequest = record.MakeDefRequest();
                if (TryMergeWithExistingEntry(record))
                {
                    continue;
                }
                if (TryAddNewEntry(record))
                {
                    continue;
                }
                if(RV2Log.ShouldLog(false, Common.LogCategory))
                    RV2Log.Message($"Could not find a drawer for record {record}", Common.LogCategory);
            }
            return entries;

            bool TryMergeWithExistingEntry(VoreTrackerRecord record)
            {
                VoreDrawerDef matchingExistingDrawer = entries.Keys.FirstOrDefault(def => def.AppliesTo(drawerDefRequest));
                if (matchingExistingDrawer == null)
                {
                    return false;
                }
                entries[matchingExistingDrawer].Add(record);
                if (RV2Log.ShouldLog(false, Common.LogCategory))
                    RV2Log.Message($"Merged record {record} into existing drawer def {matchingExistingDrawer}", Common.LogCategory);
                return true;
            }

            bool TryAddNewEntry(VoreTrackerRecord record)
            {
                IEnumerable<VoreDrawerDef> applicableDrawers = DefDatabase<VoreDrawerDef>.AllDefsListForReading
                        .Where(def =>
                        {
                            AcceptanceReport report = def.AppliesTo(drawerDefRequest);
                            if (RV2Log.ShouldLog(true, Common.LogCategory))
                                RV2Log.Message($"VoreDrawerDef {def.defName} applicable ? {(report.Accepted ? "YES" : report.Reason)}", Common.LogCategory);
                            return report;
                        });
                if(applicableDrawers.EnumerableNullOrEmpty())
                {
                    return false;
                }
                VoreDrawerDef newDrawerDef = applicableDrawers
                        .MaxBy(def => def.priority);
                entries.Add(newDrawerDef, new List<VoreTrackerRecord>() { record });
                if (RV2Log.ShouldLog(false, Common.LogCategory))
                    RV2Log.Message($"Added new def {newDrawerDef} for record {record}", Common.LogCategory);
                return true;
            }
        }

        public static float GetDigestionProgress(VoreTrackerRecord record)
        {
            PartGoalDef currentGoal = record.CurrentVoreStage?.def?.partGoal;
            if(currentGoal == null)
            {
                return 0f;
            }
            PartGoalExtension_DigestionProgress extension = currentGoal.GetModExtension<PartGoalExtension_DigestionProgress>();
            if(extension == null)
            {
                return 0f;
            }
            return extension.digestionProgress;
        }

        private static DrawerDefRequest MakeDefRequest(this VoreTrackerRecord record)
        {
            return new DrawerDefRequest()
            {
                partName = record.CurrentVoreStage.def.partName,
                predator = record.Predator
            };
        }

        //public static IEnumerable<DrawerStageProgress> GetStageProgressList(IEnumerable<VoreTrackerRecord> records)
        //{
        //    foreach (VoreTrackerRecord record in records)
        //    {
        //        yield return ParseProgressFromPartGoal(record.CurrentVoreStage.def.partGoal);
        //    }
        //}

        //public static DrawerStageProgress ParseProgressFromPartGoal(string partGoal)
        //{
        //    if(!Enum.TryParse(partGoal, true, out DrawerStageProgress result))
        //    {
        //        return DrawerStageProgress.Intact;
        //    }
        //    return result;
        //}
    }
}
