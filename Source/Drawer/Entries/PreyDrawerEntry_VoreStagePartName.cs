﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_VoreStagePartName : PreyDrawerEntry
    {
        bool inverted = false;
        List<string> partNames = new List<string>();
        List<string> displayPartNames = new List<string>();
        List<string> displayPartTranslationKeys = new List<string>();

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            VoreStageDef actualStage = records.First().CurrentVoreStage.def;

            bool applies = partNames.Contains(actualStage.partName)
                || displayPartNames.Contains(actualStage.displayPartName)
                || displayPartTranslationKeys.Contains(actualStage.displayPartTranslationKey);

            if (inverted)
            {
                return !applies;
            }
            return applies;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if(partNames.NullOrEmpty() && displayPartNames.NullOrEmpty() && displayPartTranslationKeys.NullOrEmpty())
            {
                yield return $"one of {nameof(partNames)}, {nameof(displayPartNames)}, {nameof(displayPartTranslationKeys)} must be provided";
            }
        }
    }
}
