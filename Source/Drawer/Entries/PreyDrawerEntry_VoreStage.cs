﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_SAR_Bulges
{
    public class PreyDrawerEntry_VoreStage : PreyDrawerEntry
    {
        bool inverted = false;
        List<VoreStageDef> stages;

        public override AcceptanceReport AppliesTo(List<VoreTrackerRecord> records)
        {
            VoreStageDef actualStage = records.First().CurrentVoreStage.def;

            bool applies = stages.Contains(actualStage);

            if (inverted)
            {
                return !applies;
            }
            return applies;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
                yield return error;
            if (stages.NullOrEmpty())
                yield return $"Dictionary \"{nameof(stages)}\" is null or empty";
        }
    }
}
