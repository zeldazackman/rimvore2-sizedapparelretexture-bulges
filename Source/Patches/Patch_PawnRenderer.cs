﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RV2_SAR_Bulges
{
    [HarmonyPatch(typeof(PawnRenderer), "DrawPawnBody")]
    public static class Patch_PawnRenderer
    {
        [HarmonyPostfix]
        public static void DrawVoreBulges(Vector3 rootLoc, float angle, PawnRenderFlags flags, Pawn ___pawn)
        {
            Rot4 pawnRotation = ___pawn.Rotation;
            try
            {
                if(!ShouldDrawBulgesFor(___pawn))
                {
                    return;
                }

                ___pawn.GetBulgePawnData()?.drawer?.DrawAt(rootLoc, angle, pawnRotation, flags);
            }
            catch (Exception e)
            {
                Log.Error($"Caught unhandled exception: {e}");
            }
        }

        public static bool ShouldDrawBulgesFor(Pawn pawn)
        {
            return true;
        }
    }
}
