﻿using RimVore2;
using RimWorld;
using RV2_SAR_Bulges;
using SizedApparel;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace RV2_SAR_Bulges
{
    public class VoreTrackerDrawer : IExposable
    {
        private Pawn predator;
        int rememberedTweakDrawerUtiltiesHash = -1;

        public VoreTrackerDrawer() { }

        public VoreTrackerDrawer(Pawn pawn)
        {
            this.predator = pawn;
        }
        
        List<GraphicData> preparedGraphicDatas = new List<GraphicData>();
        List<Verse.Graphic> graphicsToDraw = new List<Verse.Graphic>();

        public void Recache()
        {
            if (RV2Log.ShouldLog(false, Common.LogCategory))
                RV2Log.Message($"Recaching drawers for {predator.LabelShort}", Common.LogCategory);
            graphicsToDraw.Clear();
            preparedGraphicDatas.Clear();

            VoreTracker tracker = predator.PawnData()?.VoreTracker;
            if(tracker == null)
            {
                return;
            }
            Dictionary<VoreDrawerDef, List<VoreTrackerRecord>> drawersWithRequests = DrawerRequestUtility.MakeDrawersWithRequests(tracker);
            if(RV2Log.ShouldLog(false, Common.LogCategory))
            {
                string stringifiedEntries = drawersWithRequests.NullOrEmpty() ? "NONE" : string.Join("\n", drawersWithRequests.Select((kvp => $"{kvp.Key.defName}: {kvp.Value.Select(rec => $"{rec.Prey}: {rec.CurrentVoreStage.def.defName}").ToStringSafeEnumerable()}")));
                RV2Log.Message($"Produced the following drawers with entries: \n{stringifiedEntries}", Common.LogCategory);
            }
            if (drawersWithRequests.NullOrEmpty())
            {
                return;
            }
            preparedGraphicDatas = GetGraphicDatas(drawersWithRequests).ToList();
            if(RV2Log.ShouldLog(false, Common.LogCategory))
            {
                string stringifiedGraphics = preparedGraphicDatas.NullOrEmpty() ? "NONE" : string.Join(", ", preparedGraphicDatas.Select(g => g.texPath));
                RV2Log.Message($"Retrieved the following graphics: {stringifiedGraphics}", Common.LogCategory);
            }
        }

        private IEnumerable<GraphicData> GetGraphicDatas(Dictionary<VoreDrawerDef, List<VoreTrackerRecord>> entries)
        {
            foreach (KeyValuePair<VoreDrawerDef, List<VoreTrackerRecord>> entry in entries)
            {
                foreach (GraphicData graphicData in entry.Key.GetGraphicsFor(entry.Value))
                {
                    yield return graphicData;
                }
            }
        }

        public void DrawAt(Vector3 rootLoc, float angle, Rot4 pawnRotation, PawnRenderFlags flags)
        {
            RecacheIfTweakValuesApply();
            FinalizePreparedGraphicDatas();

            foreach (Verse.Graphic graphic in graphicsToDraw)
            {
                bool drawNow = flags.FlagSet(PawnRenderFlags.DrawNow);
                DrawMeshForGraphicWithProperRotation(graphic, pawnRotation, rootLoc, angle, drawNow);
            }
        }

        private void DrawMeshForGraphicWithProperRotation(Graphic graphic, Rot4 rotation, Vector3 location, float angle, bool drawNow)
        {
            Vector3 offset = graphic.DrawOffset(rotation);
            offset = offset.RotatedBy(angle);
            location += offset;
            Mesh mesh = graphic.MeshAt(rotation);
            Material material = graphic.MatAt(rotation);
            Quaternion angleQuaternion = Quaternion.AngleAxis(angle, Vector3.up);
            GenDraw.DrawMeshNowOrLater(mesh, location, angleQuaternion, material, drawNow);
        }

        private void FinalizePreparedGraphicDatas()
        {
            if (preparedGraphicDatas.NullOrEmpty())
            {
                return;
            }
            if(RV2Log.ShouldLog(false, Common.LogCategory))
            {
                string stringifiedGraphicDatas = string.Join(", ", preparedGraphicDatas.Select(g => g.texPath));
                RV2Log.Message($"Finalizing prepared graphics: {stringifiedGraphicDatas}", Common.LogCategory);
            }
            for (int i = 0; i < preparedGraphicDatas.Count; i++)
            {
                GraphicData preparedGraphicData = ModifyGraphicDataForPredator(preparedGraphicDatas[i]);
                if (Common.TweakTextureDef != null && Common.TweakTextureDef.AppliesTo(preparedGraphicData))
                {
                    preparedGraphicData = DrawerTweakUtility.CloneAndOverwriteOffsets(preparedGraphicData);
                }

                Graphic finalizedGraphic;
                if(predator.story != null)
                {
                    Color predatorSkinColor = predator.story.SkinColor;
                    finalizedGraphic = preparedGraphicData.Graphic.GetColoredVersion(preparedGraphicData.shaderType.Shader, predatorSkinColor, predatorSkinColor);
                }
                else
                {
                    finalizedGraphic = preparedGraphicData.Graphic;
                }
                graphicsToDraw.Add(finalizedGraphic);
            }
            preparedGraphicDatas.Clear();
        }

        private GraphicData ModifyGraphicDataForPredator(GraphicData graphicData)
        {
            GraphicData processedGraphicData = new GraphicData();
            processedGraphicData.CopyFrom(graphicData);
            processedGraphicData.drawSize *= predator.story.bodyType.bodyGraphicScale;
            return processedGraphicData;
        }

        private void RecacheIfTweakValuesApply()
        {
            if (graphicsToDraw.NullOrEmpty())
            {
                return;
            }
            int newHash = DrawerTweakUtility.HashSum();
            if (rememberedTweakDrawerUtiltiesHash == -1)
            {
                rememberedTweakDrawerUtiltiesHash = newHash;
            }
            if (rememberedTweakDrawerUtiltiesHash != newHash)
            {
                if(RV2Log.ShouldLog(true, Common.LogCategory))
                    RV2Log.Message($"Recaching due to tweakvalue change", Common.LogCategory);
                rememberedTweakDrawerUtiltiesHash = newHash;
                Recache();
            }
        }

        public void ExposeData()
        {
            Scribe_References.Look(ref predator, nameof(predator));
        }
    }
}
